let itbMenuButton = document.getElementById("itbMenuButton");

let itbToggleBar = document.getElementById("itbToggleBar"),
	openMenu = document.getElementById("openMenu"),
	closeMenu = document.getElementById("closeMenu");

const typeWriter = (text, idName, speed, delay) => {
	let idForTypeWriter, i, splitText, typeInterval;
	const typeFunction = () => {
		idForTypeWriter = document.getElementById(idName);
		idForTypeWriter.innerHTML = "";
		splitText = text.split("");
		i = 0;
		const inputText = () => {
			if (i != splitText.length) {
				idForTypeWriter.innerHTML += splitText[i];	
			} else {
				clearInterval(typeInterval);
			}
			i++;
		}
		typeInterval = setInterval(inputText, speed);
	}
	setTimeout(typeFunction, delay);
}

let itbDesc = "ITBook offers courses related with\
	information technology such as networking, programming, data analyzing and etc.."
typeWriter(itbDesc,"itbDisplay", 50, 5000);

itbMenuButton.onclick = () => {
	if (itbToggleBar.style.height == "50px") {
		itbToggleBar.style.height = "auto";
		openMenu.style.display = "none";
		closeMenu.style.display = "inline";
	} else {
		itbToggleBar.style.height = "50px";
		openMenu.style.display = "inline";
		closeMenu.style.display = "none";
	}
}

let navItems =  document.querySelector("#navSession");
let userToken = localStorage.getItem("token");
let registerLink =  document.querySelector("#registerLink");


if(!userToken){
	registerLink.style.display = "flex";
	navItems.innerHTML = 
	`
		<li id="logInLink" class="tooltip">
			<a id="logInWord" class="itb-nav-link" href="./pages/login.html">LOG IN</a>
		</li>
	`
}else{
	registerLink.style.display = "none";
	let userEmail = localStorage.getItem("email");
	let isAdmin = localStorage.getItem("isAdmin");
	if (isAdmin == "true") {
		navItems.innerHTML = 
		`
			<li class="userButton">
				<a id="userName" class="itb-nav-link" href="#">${userEmail}</a>
				<div id="userMenu">
					<div class="user-menu">
						<a href="./pages/database.html">
							DATABASE
						</a>
					</div>
					<div class="user-menu">
						<a href="./pages/logout.html">
							LOG OUT
						</a>
					</div>
				</div>	
			</li>	
		`
	} else {
		navItems.innerHTML = 
		`
			<li class="userButton">
				<a id="userName" class="itb-nav-link" href="#">${userEmail}</a>
				<div id="userMenu">
					<div class="user-menu">
						<a href="./pages/profile.html">
							PROFILE
						</a>
					</div>
					<div class="user-menu">
						<a href="./pages/logout.html">
							LOG OUT
						</a>
					</div>
				</div>	
			</li>	
		`
	}
	let userName = document.getElementById("userName");

	userName.onclick = () => {
		let userMenu = document.getElementById("userMenu");
		if (userMenu.style.display = "none") {
			userMenu.style.display = "block";
		} else {
			userMenu.style.display = "none";
		}
	}
}



