let token = localStorage.getItem("token");
let isAdmin = localStorage.getItem("isAdmin");
let enrollButton = document.getElementById("enrollButton");
let titleList = document.getElementById("titleList");

let params = new URLSearchParams(window.location.search)
let courseId = params.get('courseId')

let userId = localStorage.getItem("id");

let courseName = document.querySelector("#courseName");
let courseDesc = document.querySelector("#description");
let coursePrice = document.querySelector("#price");
let enrollees = document.querySelector("#enrollees");

if (isAdmin == "false") {
	titleList.style.display = "none";
	enrollees.style.display = "none";
	fetch("https://njoe-capstone-2.herokuapp.com/api/users/details", {
		method : "GET",
		headers: {
					'Content-Type': 'application/json',
					'Authorization': `Bearer ${token}`
				}
	})
	.then(res => res.json())
	.then(data => {
		let userCoursesId = data.enrollments;
		let findCourseId;
		for(findCourseId of userCoursesId) {
			if(findCourseId.courseId === courseId) {
				alertBackground.style.display = "flex";
				alertSection.innerHTML =
				`
					<p>You already enrolled on this course.</p>
					<div id="alertButtons">
						<a class="alert-button" href="./courses.html" onclick = "closeAlert()">Leave this page</a>
					</div>
				`
				break;
			}
		}
		fetch(`https://njoe-capstone-2.herokuapp.com/api/courses/${courseId}`)
		.then((res) => res.json())
		.then((data) => {
			courseName.innerHTML = data.name
			courseDesc.innerHTML = data.description
			coursePrice.innerHTML = "&#8369; " + data.price
			enrollButton.addEventListener("click", () => {
				//enroll the user for the course
				fetch('https://njoe-capstone-2.herokuapp.com/api/users/enroll', {
					method: "PUT",
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					},
					body: JSON.stringify({
						courseId: courseId,
						userId: userId
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					alertBackground.style.display = "flex";
					if(data === true){
						alertSection.innerHTML =
						`
							<p>You are successfully enrolled on ${courseName.innerHTML}</p>
							<div id="alertButtons">
								<a class="alert-button" href="./courses.html" onclick = "closeAlert()">OK</a>
							</div>
						`
					}else{
						alertSection.innerHTML =
						`
							<p>You are successfully enrolled on ${courseName.innerHTML}</p>
							<div id="alertButtons">
								<a class="alert-button" href="./course.html?${courseId}" onclick = "closeAlert()">Stay on this page</a>
								<a class="alert-button" href="./courses.html" onclick = "closeAlert()">Leave this page</a>
							</div>
						`
					}
				})
			})
		})
	})	
} else {
	enrollButton.style.display = "none";
	fetch(`https://njoe-capstone-2.herokuapp.com/api/courses/${courseId}`)
	.then((res) => res.json())
	.then((data) => {
		courseName.innerHTML = data.name
		courseDesc.innerHTML = data.description
		coursePrice.innerHTML = "&#8369; " + data.price
		let studentsData;
		if(data.enrollees.length < 1) {
			enrollees.innerHTML += 
			`
				<tr>
					<td colspan = "5" style="text-align: center;">
						No Students Enrolled
					</td>
				</tr>
			`
		} else {
			studentsData = data.enrollees.map( studentData => {
				fetch(`https://njoe-capstone-2.herokuapp.com/api/users/${studentData.userId}`, {
					method: "GET",
					headers: {
						'Content-Type': 'application/json',
						'Authorization': `Bearer ${token}`
					}
				})
				.then(res => res.json())
				.then(data => {
					let studentCourse, studentDateEnrolledOn, studentStatusOnCourse;
					for (studentCourse of data.enrollments) {
						if (studentCourse.courseId == courseId){
							studentDateEnrolledOn = studentCourse.enrolledOn;
							studentStatusOnCourse = studentCourse.status;
							break;
						}
					}
					enrollees.innerHTML +=
					`
						<tr>
							<td>
								${data.firstName} ${data.lastName}
							</td>
							<td>
								${data.email}
							</td>
							<td>
								${data.mobileNo}
							</td>
							<td>
								${new Date(studentDateEnrolledOn).toLocaleString()}
							</td>
							<td>
								${studentStatusOnCourse}
							</td>
						</tr>
					`
				})
			})		
		}
	
	})
}
