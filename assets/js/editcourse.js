// instantiate a URLSearchParams object so we can execute methods to access 
// specific parts of the query string
let params = new URLSearchParams(window.location.search);

// get method returns the value of the key passed in as an argument

let courseId = params.get('courseId')

let name = document.querySelector("#courseName")
let price = document.querySelector("#price")
let description = document.querySelector("#description")

fetch(`https://njoe-capstone-2.herokuapp.com/api/courses/${courseId}`)
.then(res => {
    return res.json()
})
.then(data => {
	// assign the current values as placeholders
	name.placeholder = data.name
	price.placeholder = data.price
	description.innerText = data.description

	document.querySelector("#editCourseForm").addEventListener("submit", (e) => {
		e.preventDefault()

		let courseName = name.value
		let desc = description.value
		let priceValue = price.value

        if(name.value === ""){
            courseName = name.placeholder
        }

        if(price.value === ""){
            priceValue = price.placeholder
        }

		let token = localStorage.getItem('token')

		fetch('https://njoe-capstone-2.herokuapp.com/api/courses', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({
                courseId: courseId,
                name: courseName,
                description: desc,
                price: priceValue
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            if(data === true){
                alertBackground.style.display = "flex";
                alertSection.innerHTML =
                `
                    <p>Successfully Edited</p>
                    <div id="alertButtons">
                        <a class="alert-button" href="./courses.html" onclick = "closeAlert()">Go To Courses Page</a>
                    </div>
                `
            }else{
                alertBackground.style.display = "flex";
                alertSection.innerHTML =
                `
                    <p>Something went wrong. Kindly try again.</p>
                    <div id="alertButtons">
                        <a class="alert-button" href="./editcourse.html?${courseId}" onclick = "closeAlert()">OK</a>
                    </div>
                `
            }
        })
	})
})