let navItems =  document.querySelector("#navSession");
let userToken = localStorage.getItem("token");
let registerLink =  document.querySelector("#registerLink");

if(!userToken){
	navItems.innerHTML = 
	`
		<li id="logInLink" class="tooltip">
			<a id="logInWord" class="itb-nav-link" href="./login.html">LOG IN</a>
		</li>
	`
}else{
	registerLink.style.display = "none";
	let userEmail = localStorage.getItem("email")
	navItems.innerHTML = 
	`
		<li class="userButton">
			<a id="userName" class="itb-nav-link" href="#">${userEmail}</a>
			<div id="userMenu">
				<div class="user-menu">
					<a href="./profile.html">
						PROFILE
					</a>
				</div>
				<div class="user-menu">
					<a href="./logout.html">
						LOG OUT
					</a>
				</div>
			</div>	
		</li>	
	`

	let userName = document.getElementById("userName");

	userName.onclick = () => {
		let userMenu = document.getElementById("userMenu");
		if (userMenu.style.display == "none") {
			userMenu.style.display = "block";
		} else {
			userMenu.style.display = "none";
		}
	}
}