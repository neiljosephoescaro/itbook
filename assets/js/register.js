let registerForm = document.querySelector("#registerForm")

registerForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let firstName = document.querySelector("#firstName").value;
	let lastName = document.querySelector("#lastName").value;
	let mobileNo = document.querySelector("#mobileNumber").value;
	let email = document.querySelector("#emailAddress").value;
	let password1 = document.querySelector("#passWord").value;
	let password2 = document.querySelector("#passWordVerified").value;
	if(password1 === password2){
		fetch('https://njoe-capstone-2.herokuapp.com/api/users/email-exists', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			//if no existing email is found
			if(data === false){
				fetch('https://njoe-capstone-2.herokuapp.com/api/users', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					alertBackground.style.display = "flex";
					if(data === true){
						alertSection.innerHTML =
						`
							<p>You successfully registered.</p>
							<div id="alertButtons">
								<a class="alert-button" href="./register.html" onclick = "closeAlert()">Stay On This Page</a>
								<a class="alert-button" href="./login.html" onclick = "closeAlert()">Go To Log In</a>
							</div>
						`
					}else{
						alertSection.innerHTML =
						`
							<p>Something went wrong. Be careful with your inputs</p>
							<div id="alertButtons">
								<a class="alert-button" href="./register.html" onclick = "closeAlert()">OK</a>
							</div>
						`
					}
				})
			} else {
				alertBackground.style.display = "flex";
				alertSection.innerHTML =
				`
					<p>Sorry but your e-mail already used.</p>
					<div id="alertButtons">
						<a class="alert-button" href="#itbToggleBar" onclick = "closeAlert()">OK</a>
					</div>
				`
			}
		})
	}else{
		alertBackground.style.display = "flex";
		alertSection.innerHTML =
		`
			<p>Your passwords didn't match</p>
			<div id="alertButtons">
				<a class="alert-button" href="#itbToggleBar" onclick = "closeAlert()">OK</a>
			</div>
		`
	}
});