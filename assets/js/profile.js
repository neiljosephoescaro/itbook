let profileToken = localStorage.getItem("token");
let completeName = document.getElementById("completeName");
let emailAddress = document.getElementById("emailAddress");
let mobileNumber = document.getElementById("mobileNo");
let userCourses = document.getElementById("userCourses");
fetch("https://njoe-capstone-2.herokuapp.com/api/users/details",{
	method : "GET",
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${profileToken}`
	}
})
.then(res => res.json())
.then( data => {
	document.title = data.firstName + " " + data.lastName;
	completeName.innerHTML = data.firstName + " " + data.lastName;
	emailAddress.innerHTML = data.email;
	mobileNumber.innerHTML = data.mobileNo;
	for(userCourse of data.enrollments) {
		let courseStatus = userCourse.status;
		fetch(`https://njoe-capstone-2.herokuapp.com/api/courses/${userCourse.courseId}`)
		.then(res => res.json())
		.then(data =>{
			let userCourse, courseName, courseDescription;
			courseName = data.name;
			courseDescription = data.description;
			userCourses.innerHTML +=
			`
				<tr>
					<td>
						${courseName}
					</td>
					<td>
						${courseDescription}
					</td>
					<td>
						${courseStatus}
					</td>
				</tr>
			`
		})
	}
})