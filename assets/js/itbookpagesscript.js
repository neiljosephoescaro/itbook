let itbMenuButton = document.getElementById("itbMenuButton");

itbMenuButton.onclick = () => {
	if (itbToggleBar.style.height == "50px") {
		itbToggleBar.style.height = "auto";
		openMenu.style.display = "none";
		closeMenu.style.display = "inline";
	} else {
		itbToggleBar.style.height = "50px";
		openMenu.style.display = "inline";
		closeMenu.style.display = "none";
	}
}

let navItems =  document.querySelector("#navSession");
let userToken = localStorage.getItem("token");

if(!userToken){
	navItems.innerHTML = 
	`
		<li id="logInLink" class="tooltip">
			<a id="logInWord" class="itb-nav-link" href="./login.html">LOG IN</a>
		</li>
	`
}else{
	let userEmail = localStorage.getItem("email");
	let isAdmin = localStorage.getItem("isAdmin");
	if (isAdmin == "true") {
		navItems.innerHTML = 
		`
			<li class="userButton">
				<a id="userName" class="itb-nav-link" href="#">${userEmail}</a>
				<div id="userMenu">
					<div class="user-menu">
						<a href="./database.html">
							DATABASE
						</a>
					</div>
					<div class="user-menu">
						<a href="./logout.html">
							LOG OUT
						</a>
					</div>
				</div>	
			</li>	
		`
	} else {
		navItems.innerHTML = 
		`
			<li class="userButton">
				<a id="userName" class="itb-nav-link" href="#">${userEmail}</a>
				<div id="userMenu">
					<div class="user-menu">
						<a href="./profile.html">
							PROFILE
						</a>
					</div>
					<div class="user-menu">
						<a href="./logout.html">
							LOG OUT
						</a>
					</div>
				</div>	
			</li>	
		`
	}

	let userName = document.getElementById("userName");

	userName.onclick = () => {
		let userMenu = document.getElementById("userMenu");
		if (userMenu.style.display == "none") {
			userMenu.style.display = "block";
		} else {
			userMenu.style.display = "none";
		}
	}
}

let alertBackground = document.querySelector("#alertBackground");
let alertSection = document.querySelector("#alertSection");

const closeAlert = () => {
	alertBackground.style.display = "none";
}

