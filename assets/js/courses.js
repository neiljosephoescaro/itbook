let registerLink =  document.querySelector("#registerLink");
let linkTo;
if (!userToken) {
	linkTo = "./login.html";
} else {
	linkTo = "./course.html";
	registerLink.style.display = "none";
}

let adminUser = localStorage.getItem("isAdmin")
let addCourseButton = document.querySelector("#adminButtons")
let container = document.querySelector("#coursesSection")
let cardFooter;

const archiveCourse = (courseId) => {
	let token = localStorage.getItem("token");

fetch(`https://njoe-capstone-2.herokuapp.com/api/courses/${courseId}`, {
	method : "DELETE",
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then( res => res.json())
.then( data => {
	if (data === true) {
		window.location.replace("./courses.html")
	}
})
}

const reActivateCourse = (courseId) => {
	let token = localStorage.getItem("token");

fetch(`https://njoe-capstone-2.herokuapp.com/api/courses/${courseId}`, {
	method : "PUT",
	headers: {
		'Content-Type': 'application/json',
		'Authorization': `Bearer ${token}`
	}
})
.then( res => res.json())
.then( data => {
	if (data === true) {
		window.location.replace("./courses.html")
	}
})
}

if(adminUser == "false" || !adminUser){
	addCourseButton.innerHTML =
	`
		<h1 style="font-family: 'Kufam', cursive; color: rgb(45,255,255); display: block;">
			AVAILABLE COURSES
		</h1>
	`;
	//fetch the courses from our API
	fetch('https://njoe-capstone-2.herokuapp.com/api/courses/active')
	.then(res => res.json())
	.then(data => {
		let courseData;

		//if the number of courses fetched is less than 1, display no courses available.
		if(data.length < 1){
			courseData = "No courses available."
		}else{
			courseData = data.map(course => {
				cardFooter = 
				`
					<a href="${linkTo}?courseId=${course._id}" id="selectButton">Select Course</a>
				`
				return (
				`
					<div class="course-section">
						<h2 class="course-title">${course.name}</h2>
						<br>
						<h3 class="course-desc">${course.description}</h3>
						<br>
						<div class="price-and-button-section">
							<h3 class="price">&#8369; ${course.price}</h3>
							<hr>
							<div class="course-buttons">
								${cardFooter}
							</div>
						</div>
					</div>
				`
				)

			}).join("")
		}
		container.innerHTML += courseData;
	})
}else{
	addCourseButton.innerHTML = 
	`
		<a href="./addcourse.html">
			<i id="addCourseButton" class="fas fa-plus"></i>
		</a>
	`
	let token = localStorage.getItem("token");
	fetch('https://njoe-capstone-2.herokuapp.com/api/courses', {
		method: "GET",
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		// console.log(data)
		let courseData;

		//if the number of courses fetched is less than 1, display no courses available.
		if(data.length < 1){
			courseData = "No courses available."
		}else{
			courseData = data.map(course => {
				let linkStatus, statusTo, statusStyle, statusSwitchTo;
				if(course.isActive === true) {
					// linkStatus = `./archive.html?courseId=${course._id}`;
					statusTo = "DISABLE";
					statusStyle = "course-button";
					statusSwitchTo = `archiveCourse("${course._id}")`;
				} else {
					// linkStatus = `./reactivate.html?courseId=${course._id}`;
					statusTo = "ENABLE";
					statusStyle = "archive-button";
					statusSwitchTo = `reActivateCourse("${course._id}")`;
				}
				cardFooter = 
				`
					<a class="course-button" href="./course.html?courseId=${course._id}">VIEW</a>
					<a class="course-button" href="./editcourse.html?courseId=${course._id}">EDIT</a>
					<a class="${statusStyle}" href="#" onclick='${statusSwitchTo}'>${statusTo}</a>
				`
				return (
				`
					<div class="course-section">
						<h2 class="course-title">${course.name}</h2>
						<br>
						<h3 class="course-desc">${course.description}</h3>
						<br>
						<div class="price-and-button-section">
							<h3 class="price">&#8369; ${course.price}</h3>
							<hr>
							<div class="course-buttons">
								${cardFooter}
							</div>
						</div>
					</div>
				`
				)

			}).join("")
		}	
		container.innerHTML += courseData
	})
}
