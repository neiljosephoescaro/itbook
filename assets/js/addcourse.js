let formSubmit = document.querySelector("#addCourseForm")

formSubmit.addEventListener("submit", (e) => {
	e.preventDefault()

	//get the value of #courseName and assign it to the courseName variable
	let courseName = document.querySelector("#courseName")
	//get the value of #courseDescription and assign it to the description variable
	let description = document.querySelector("#description")
	//get the value of #price and assign it to the price variable
	let price = document.querySelector("#price")
	//retrieve the JSON Web Token stored in our local storage for authentication
	let token = localStorage.getItem('token')

	fetch('https://njoe-capstone-2.herokuapp.com/api/courses', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json',
			'Authorization': `Bearer ${token}`
		},
		body: JSON.stringify({
			name: courseName.value,
			description: description.value,
			price: price.value
		})
	})
	.then(res => {
		return res.json()
	})
	.then(data => {
		if(data === true){
			alertBackground.style.display = "flex";
            alertSection.innerHTML =
            `
                <p>Successfully Added</p>
                <div id="alertButtons">
                	<a class="alert-button" href="./addcourse.html" onclick = "closeAlert()">Add More</a>
                    <a class="alert-button" href="./courses.html" onclick = "closeAlert()">Go To Courses Page</a>
                </div>
            `
			
		}else{
			alertBackground.style.display = "flex";
			alertSection.innerHTML =
			`
			    <p>Something went wrong. Kindly try again.</p>
			    <div id="alertButtons">
			        <a class="alert-button" href="./addcourse.html" onclick = "closeAlert()">OK</a>
			    </div>
			`
		}
	})
})