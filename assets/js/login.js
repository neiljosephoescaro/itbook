let loginForm = document.querySelector("#userLogInForm");

loginForm.addEventListener("submit", (e) => {
	e.preventDefault()

	let email = document.querySelector("#emailAddress").value
	let password = document.querySelector("#passWord").value

	if(email === "" && password === ""){
		alertBackground.style.display = "flex";
		alertSection.innerHTML =
		`
			<p>Kindly input your email account and password to log in.</p>
			<div id="alertButtons">
				<a class="alert-button" href="#itbToggleBar" onclick = "closeAlert()">OK</a>
			</div>
		`
	} else if (email === "") {
		alertBackground.style.display = "flex";
		alertSection.innerHTML =
		`
			<p>Kindly input your email account.</p>
			<div id="alertButtons">
				<a class="alert-button" href="#itbToggleBar" onclick = "closeAlert()">OK</a>
			</div>
		`
	} else if (password === "") {
		alertBackground.style.display = "flex";
		alertSection.innerHTML =
		`
			<p>Kindly input your password.</p>
			<div id="alertButtons">
				<a class="alert-button" href="#itbToggleBar" onclick = "closeAlert()">OK</a>
			</div>
		`
	}else{
		fetch('https://njoe-capstone-2.herokuapp.com/api/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			if(data.access){
				//set JWT in local storage
				localStorage.setItem('token', data.access)
				fetch('https://njoe-capstone-2.herokuapp.com/api/users/details', {
					headers: {
						Authorization:  `Bearer ${data.access}`
					}
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					//set the global user state to have properties containing the authenticated user's ID and role
					localStorage.setItem("id", data._id)
					localStorage.setItem("email",data.email)
					localStorage.setItem("isAdmin", data.isAdmin)
					window.location.replace("./courses.html")
				})
			} else {
				alertBackground.style.display = "flex";
				alertSection.innerHTML =
				`
					<p>Your email is not registered or wrong password.</p>
					<div id="alertButtons">
						<a class="alert-button" href="#itbToggleBar" onclick = "closeAlert()">OK</a>
					</div>
				`
			}
		})
	}
})